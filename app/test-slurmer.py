import requests

headers = {'X-Auth-Token':'averycomplicatedchallenge', 'User':'slurmer'}

response = requests.get('http://10.40.41.40:8080/apps/', headers=headers)
print(response)
print(response.json())

response = requests.get('http://10.40.41.40:8080/apps/a9a5fc66-9bed-4a13-874a-d8d7d1756224/', headers=headers)
print(response)
print(response.json())


response = requests.get('http://10.40.41.40:8080/apps/a9a5fc66-9bed-4a13-874a-d8d7d1756224/jobs', headers=headers)
print(response)
print(response.json())

response = requests.post('http://10.40.41.40:8080/apps/a9a5fc66-9bed-4a13-874a-d8d7d1756224/jobs', headers=headers)
print(response)

