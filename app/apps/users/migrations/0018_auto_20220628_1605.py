# Generated by Django 2.2.24 on 2022-06-28 16:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0017_quotaevent'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='onboarding',
            field=models.BooleanField(default=False, verbose_name='Show onboarding'),
        ),
    ]
