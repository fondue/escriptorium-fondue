# Generated by Django 2.2.24 on 2021-11-23 17:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0060_clusterjob_created_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='clusterjob',
            name='ocr_model',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='cluster_jobs', to='core.OcrModel'),
        ),
    ]
