# Generated by Django 2.2.24 on 2021-11-24 15:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0064_ocrmodel_created_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='submitting_job',
            field=models.BooleanField(default=False),
        ),
    ]
